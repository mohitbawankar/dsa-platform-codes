
/*

Approach:

    Take a hashset and add the element into it only if the elements/value are repeated and simultaneously add those elements into the arraylist .
    Note: .add() method in hashset checks if the element is unique,if it finds the the elements in repeated it won't add it to hashset
    but we applied a negative condition of it so that it would only add those elements into it which are repeated.

*/


class Solution {
    public List<Integer> findDuplicates(int[] nums) {

        HashSet<Integer> hs = new HashSet<>();
        ArrayList<Integer> al = new ArrayList<>();

        for (int i : nums) {
            if (!hs.add(i)) {
                al.add(i);
            }
        }
        return al;
    }
}

