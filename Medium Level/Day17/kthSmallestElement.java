
/*
    Approach used to solve the question:

    1. Sort the array so that we get the minimum values at the beginning.
    2. Then return the (k-1)th indexed value to get the kth smallest value within the array.

*/
class Solution{
    public static int kthSmallest(int[] arr, int l, int r, int k) {

        Arrays.sort(arr);
        return arr[k-1];

    }
}

