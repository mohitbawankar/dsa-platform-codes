
/*
    Approach:used to solve the question:

        1.  Sorted the array
        2.  Reverse the array so that while popping we get the largest number first.
             - Created a stack and pushed the array element into it
             - Then fill the original array by popping the stack into it.
        3. Now we can get the kth index at (k-1) index.

*/

class Solution {
    public int findKthLargest(int[] nums, int k) {

        Stack<Integer> s = new Stack<>();
        Arrays.sort(nums);
        for(int i = 0; i < nums.length;i++){
            s.push(nums[i]);
        }
        for(int i = 0; i < nums.length;i++){
            nums[i] = s.pop();
        }
        return nums[k-1];
    }
}
