

/*

Approach:
- Variable max is used to store the max value of each subarrays.
- Since the i is the starting position for k sized subarray so we need to bound it by -3 so that the ending position would reach ti the end.else indexoutofbounds error appears
- The j whch is used to traverse the inner loop is bounded till k so that summing their indices(i.e i+j) would result k length only.
- Inner loop check the maximum vaue and stores into it.
- Return the max as it contains the maximum value of sum array.


*/

class Solution{

    static ArrayList <Integer> max_of_subarrays(int arr[], int n, int k){

        ArrayList<Integer> al = new ArrayList<>();

        int max = Integer.MIN_VALUE;
        for (int i = 0; i <= n - k; i++) {

            max = arr[i];

            for (int j = 1; j < k; j++) {
                if (arr[i + j] > max)
                    max = arr[i + j];
            }
            al.add(max);
        }

        return al;

    }
}
