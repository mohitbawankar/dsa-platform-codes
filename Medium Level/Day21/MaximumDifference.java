

/*
Approach used:

1) Created two new arrays of leftSmall and rightSmall arrays and put the 0th and last index of leftSmall and rightSmall as 0 as there will be no value smaller than the current value if we are at 0th or lastindex of those two arrays.
2) Created a new array which stores the values of absolute difference of those two arrays.
3) Created a new variable as max which stores the maximum value in the diff array
4) Returned the max as output.

*/
class Solution{
    int findMaxDiff(int arr[], int n){

	    int smallLeft[] = new int[n];
	    int smallRight[] = new int[n];


	    for(int i = 1; i < n;i++){
	        for(int j = 0; j < i;j++){
	            if(arr[j] < arr[i]){
	                smallLeft[i] = arr[j];
	            }
	        }
	    }

	    for(int i = n-2;i >= 0;i--){
	        for(int j = n-1;j >= i;j--){
	            if(arr[i] > arr[j]){
	                smallRight[i] = arr[j];
	            }
	        }
	    }


	    int diff[] = new int[n];
	    for(int i = 0; i < n;i++){

	        diff[i] = Math.abs(smallLeft[i] - smallRight[i]);

	    }

	    int max = Integer.MIN_VALUE;

	    for(int i = 0; i < diff.length;i++){
	        if(max < diff[i]){
	            max = diff[i];
	        }
	    }


	    return max;

    }
}
