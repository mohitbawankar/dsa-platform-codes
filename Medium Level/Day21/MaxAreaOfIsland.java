
/*
    - We declare and initialize ans variable with value 0 which will store our final answer.
    - We then start two nested for loops that iterate over all elements in the grid.
    - These loops are used to traverse through every element in the grid so that we can check if it's part of any island or not.
    - Inside these loops, we call another function dfs passing it three parameters - grid, i, j.
    - This function performs depth-first search on each cell starting from index [i][j] until there are no more adjacent cells left unvisited or until we reach out-of-bounds indices.
    - Firstly, we have four base cases: - If i < 0 || i == grid.length || j < 0 || j == grid[0].length: This checks whether our current position has gone out-of-bounds by comparing row/column indices with their respective lengths.
    - If grid[i][j] != 1: This checks whether our current position contains water.
    - If yes, then there's no point continuing further since islands only consist of land ('1').
    - Otherwise (if none of above conditions hold true), then mark current cell as visited by changing its value from '1' to '2'.
    - Return the area by using dfs again.
*/

class Solution {
    public int maxAreaOfIsland(int[][] grid) {

    int ans = 0;

    for (int i = 0; i < grid.length; ++i)
      for (int j = 0; j < grid[0].length; ++j)
        ans = Math.max(ans, dfs(grid, i, j));

    return ans;
  }

  int dfs(int[][] grid, int i, int j) {
    if (i < 0 || i == grid.length || j < 0 || j == grid[0].length)
      return 0;
    if (grid[i][j] != 1)
      return 0;

    grid[i][j] = 0;

    return 1 + dfs(grid, i + 1, j) + dfs(grid, i - 1, j) + dfs(grid, i, j + 1) + dfs(grid, i, j - 1);
  }
}

