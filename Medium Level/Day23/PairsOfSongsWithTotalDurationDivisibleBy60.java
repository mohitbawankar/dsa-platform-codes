
class Solution {
    public int numPairsDivisibleBy60(int[] time) {

        int count = 0;
        int rem[] = new int[60];

        for(int i = 0; i < time.length;i++){

            count = count + rem[(600 - time[i]) % 60];
            rem[time[i] % 60] = rem[time[i] % 60] + 1;
        }
        return count;
    }
}
