
class Solution {
    public int[] productExceptSelf(int[] nums) {

        int arr[] = new int[nums.length];
        int prefixMultiplication = 1;
        int suffixMultiplication = 1;


        // to calulate the prefix multiplication of this nums array
        for(int i = 0; i < nums.length;i++){
            arr[i] = prefixMultiplication;
            prefixMultiplication = prefixMultiplication * nums[i];
        }

        // to calculate the suffix multiplication of array and multiply these values with prefixmultiplication
        for(int i = nums.length - 1;i >= 0;i--){
            arr[i] = arr[i] * suffixMultiplication;
            suffixMultiplication = suffixMultiplication * nums[i];
        }

        return arr;
    }
}
