


class Solution {
    int rowWithMax1s(int arr[][], int n, int m) {

       int index = -1;                                  // To store the index of maximum 1 row
       int maxOccurence = 0;                            // used to compare to get the maximum value of occurence of 1 in row

       for(int i = 0; i < n;i++){                       // to iterate through row
           int count = 0;                               // count the no of times the 1 occured in row

           for(int j = 0; j < m ;j++){                  // to iterate over the column

               if(arr[i][j] == 1){
                   count++;                             // increment count when we find 1 on that matrix
               }
           }

           if(count > maxOccurence){                    // check if the count is greater than max occurence
               maxOccurence = count;                    // store the count into maxoccurence
               index = i;                               // keep the index value of max occurence of 1 in that index variable

           }
       }

       return index;                                    // Return index value.
    }
}
