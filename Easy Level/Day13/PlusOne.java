
class Solution{

	int[] plusOne(int[] arr) {

        int size = arr.length;

		for (int i = size - 1; i >= 0; i--) {
            	if (arr[i] != 9) {
                	arr[i]++;
                	return arr;
           		}
	       		else {
            		arr[i] = 0;
			}
		}

        int[] result = new int[size + 1];
    	result[0] = 1;
    	return result;

	}
}
