
class Solution {
    public int missingNumber(int[] nums) {

        int n = nums.length;
        int sumNum = (n*(n+1))/2;
        int sumArr = 0;

        for(int i = 0; i < nums.length;i++){
            sumArr = sumArr + nums[i];
        }

        int missingNum = (sumNum - sumArr);


        return missingNum;
    }
}
