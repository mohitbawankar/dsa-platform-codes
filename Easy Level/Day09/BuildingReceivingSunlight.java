

class Solution {

    public static int longest(int arr[],int n){

        int count = 1;
	
	int leftMax[] = new int[n];

        leftMax[0] = arr[0];

        for(int i = 1; i < n;i++){
            if(arr[i] > leftMax[i-1]){
                leftMax[i] = arr[i];
            }else{
                leftMax[i] = leftMax[i-1];
            }
        }


        for(int i = 1; i < n;i++){

            if(arr[i] >= leftMax[i-1]){
                count++;
            }
        }

        return count;
    }
}
