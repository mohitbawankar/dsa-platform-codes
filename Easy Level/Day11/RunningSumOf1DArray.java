
class Solution {
    public int[] runningSum(int[] nums) {

        int n = nums.length;
        int pArr[] = new int[n];

        pArr[0] = nums[0];
        for(int i = 1; i < n;i++){

            pArr[i] = pArr[i-1] + nums[i];
        }

        return pArr;
    }
}
