
import java.util.*;

class Solution {
    public boolean check(int[] nums) {

        int size = nums.length;
        boolean index1 = false;
        int arr[] = new int[size];
        for(int i = 0; i < nums.length;i++){
            arr[i] = nums[i];
        }

        Arrays.sort(arr);
        for(int i = 0; i < size;i++){

            int temp = arr[size-1];
            for(int j = size - 1; j > 0;j--){

                arr[j] = arr[j-1];
            }
            arr[0] = temp;

            boolean index2 = true;
            for(int k = 0; k < nums.length;k++){
                if(arr[k] != nums[k]){
                    index2 = false;
                }
            }

            if(index2){
                index1 = true;
                break;
            }
        }
        return index1;
    }
}

class Client{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		Solution obj = new Solution();

		int size = sc.nextInt();
		int arr[] = new int[size];

		for(int i = 0; i < arr.length;i++){

			arr[i] = sc.nextInt();
		}

		boolean ret = obj.check(arr);

		System.out.println(ret);
	}
}

