
import java.util.*;

class Solution{
	int equalSum(int [] arr, int n) {

	    int equilibrium = 0;
       	    boolean flag = false;
            long sum = 0;
            for(int i = 0; i < n;i++){
                sum = sum + arr[i];
            }

            long leftSum = 0;
            long rightSum = arr[n-1];

            for(int i = 0 ; i < n;i++){
                rightSum = sum - leftSum;
                leftSum = leftSum + arr[i];

                if(leftSum == rightSum){
                    flag = true;
                    equilibrium = i;
                    break;
                }
            }

            if(n == 1){
                return 1;
            }else if(flag == false){
                return -1;
            }else{
                return (equilibrium+1);
            }

     } 
}

class Client{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		Solution obj = new Solution();

		System.out.println("Enter the size of an array");
		int size = sc.nextInt();

		System.out.println("Enter the elements in an array");
		int nums[] = new int[size];

		for(int i = 0; i < size;i++){
			nums[i] = sc.nextInt();
		}

		int ret = obj.equalSum(nums,size);
		System.out.println(ret);
	}
}

