
import java.util.*;

class Solution {
    public int pivotIndex(int[] nums) {
        int pivot = -1;
        boolean flag = false;

        for(int i = 0; i < nums.length;i++){

            int leftSum = 0;
            int rightSum = 0;

            for(int j = 0; j < i; j++){
                leftSum = leftSum + nums[j];
            }

            for(int k = i + 1; k < nums.length;k++){
                rightSum = rightSum + nums[k];
            }

            if(leftSum == rightSum){
                flag = true;
                pivot = i;
                break;
            }
        }

        if(flag == false){
            return -1;
        }else{
            return pivot;
        }

    }
}

class Client{

	public static void main(String[] args){

		Solution obj = new Solution();
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the size of an array");
		int size = sc.nextInt();

		System.out.println("Enter the elements in an array");
		int nums[] = new int[size];

		for(int i = 0; i < size;i++){
			nums[i] = sc.nextInt();

		}
		int ret = obj.pivotIndex(nums);
		System.out.println(ret);
	}
}



