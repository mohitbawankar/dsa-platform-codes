

class Solution{
	public static long[] productExceptSelf(int nums[], int n)
	{
	    long arr[] = new long[n];
        for(int i = 0;i < n;i++){
            long prod = 1;
            for(int j = 0; j < n;j++){

                if(i == j){
                    continue;
                }else{
                    prod = prod * nums[j];
                }
            }

            arr[i] = prod;


        }
        return arr;

	}
}

