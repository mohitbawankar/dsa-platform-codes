
import java.util.*;
class Solution {
    public int[] twoSum(int[] nums, int target) {
        int arr[] = new int[2];
        for(int i = 0; i < nums.length-1;i++){
            for(int j = i+1; j < nums.length;j++){
                if(nums[i] + nums[j] == target){
                    arr[0] = i;
                    arr[1] = j;
                }
            }
        }
        return arr;
    }
}


class Client{

	public static void main(String[] args){

		Solution obj = new Solution();

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the size");
		int size = sc.nextInt();

		System.out.println("Enter the target ");
		int target = sc.nextInt();

		System.out.println("Enter the elements in the array");
		int arr[] = new int[size];
		for(int i = 0; i < size;i++){
			arr[i] = sc.nextInt();
		}

		int ret[] = obj.twoSum(arr,target);

		for(int x = 0; x < ret.length;x++){
			System.out.println(ret[x]+" ");
		}
	}
}
