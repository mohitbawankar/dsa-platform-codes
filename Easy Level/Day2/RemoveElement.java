import java.util.*;
class Solution {
    public int removeElement(int[] nums, int val) {

        int temp[] = new int[nums.length];
        int cnt = 0;
        for(int i = 0 ; i < nums.length;i++){
            if(nums[i] != val){
                temp[cnt] = nums[i];
                cnt++;
            }

        }
        for(int i = 0;i < nums.length;i++){
            nums[i] = temp[i];
        }
        return cnt;
    }
}

class Client{

	public static void main(String[] args){

		Solution obj = new Solution();
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the size");
		int size = sc.nextInt();

		System.out.println("Enter the val");
		int val = sc.nextInt();

		System.out.println("Enter the elements in array");
		int arr[] = new int[size];
		for(int i = 0; i < size;i++){
			arr[i] = sc.nextInt();
		}

		int ret = obj.removeElement(arr,val);
		System.out.println(ret);
	}
}




