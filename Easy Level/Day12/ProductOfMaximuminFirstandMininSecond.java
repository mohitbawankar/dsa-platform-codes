class Solution{
    
   
    public static long find_multiplication (int arr[], int brr[], int n, int m) {
        
        long prod = 1;
        int maxEle = Integer.MIN_VALUE;
        int minEle = Integer.MAX_VALUE;
        
        for(int i = 0; i < n;i++){
            if(arr[i] > maxEle){
                maxEle = arr[i];
            }
        }
        for(int i = 0; i < m;i++){
            if(brr[i] < minEle){
                minEle = brr[i];
            }
        }
        
        prod = (maxEle * minEle);
        
        return prod;
        
    }
    
    
}
