
class Solution {
    int findMaximum(int[] arr, int n) {
        int maxEle = Integer.MIN_VALUE;

        for(int i = 0; i < n;i++){
            if(arr[i] > maxEle){
                maxEle = arr[i];
            }
        }

        return maxEle;
    }
}
